<?php

namespace TripSort\Model\Place;

/**
 * @author Safak Ciplak<safakciplak1990@gmail.com>
 */
/**
 * Class Place
 * @package TripSort\Model\Place
 */
class Place
{
    /**
     * @var string
     */
    private $name;

    /**
     * Place constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }
}

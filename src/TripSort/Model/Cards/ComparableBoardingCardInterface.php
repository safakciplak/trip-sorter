<?php

namespace TripSort\Model\Cards;

/**
 * @author Safak Ciplak<safakciplak1990@gmail.com>
 */
/**
 * Interface ComparableBoardingCardInterface
 * @package TripSort\Model\Cards
 */
interface ComparableBoardingCardInterface
{
    /**
     * @param BoardingCardInterface $card
     *
     * @return bool
     */
    public function hasSameOriginAs(BoardingCardInterface $card): bool;

    /**
     * @param BoardingCardInterface $card
     *
     * @return bool
     */
    public function hasSameDestinationAs(BoardingCardInterface $card): bool;
}

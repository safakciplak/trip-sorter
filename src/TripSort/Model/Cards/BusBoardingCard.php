<?php

namespace TripSort\Model\Cards;

use TripSort\Model\Place\Place;

/**
 * @author Safak Ciplak<safakciplak1990@gmail.com>
 */
class BusBoardingCard implements BoardingCardInterface
{
    use BoardingCardTrait;

    /**
     * BusBoardingCard constructor.
     *
     * @param Place $departurePlace
     * @param Place $arrivalPlace
     * @param string|null $seat
     */
    public function __construct(Place $departurePlace, Place $arrivalPlace, string $seat = null)
    {
        $this->departurePlace = $departurePlace;
        $this->arrivalPlace = $arrivalPlace;
        $this->seat = $seat ?: 'No seat assignment';
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf(
            'Take the airport bus from %s to %s. Seat : %s',
            $this->departurePlace,
            $this->arrivalPlace,
            $this->seat
        );
    }
}

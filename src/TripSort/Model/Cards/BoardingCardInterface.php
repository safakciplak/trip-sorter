<?php

namespace TripSort\Model\Cards;

use TripSort\Model\Place\Place;

/**
 * @author Safak Ciplak<safakciplak1990@gmail.com>
 */
interface BoardingCardInterface extends ComparableBoardingCardInterface
{
    /**
     * @return Place
     */
    public function getDeparturePlace(): Place;

    /**
     * @return Place
     */
    public function getArrivalPlace(): Place;

    /**
     * @return mixed
     */
    public function __toString();
}

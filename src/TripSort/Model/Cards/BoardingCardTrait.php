<?php

namespace TripSort\Model\Cards;

use TripSort\Model\Place\Place;

/**
 * @author Safak Ciplak<safakciplak1990@gmail.com>
 */
trait BoardingCardTrait
{
    /** @var Place */
    private $departurePlace;

    /** @var Place */
    private $arrivalPlace;

    /** @var string */
    private $seat;

    /**
     * @return Place
     */
    public function getArrivalPlace(): Place
    {
        return $this->arrivalPlace;
    }

    /**
     * @return Place
     */
    public function getDeparturePlace(): Place
    {
        return $this->departurePlace;
    }

    /**
     * @return string
     */
    public function getSeat(): string
    {
        return $this->seat;
    }

    /**
     * @param BoardingCardInterface $card
     *
     * @return bool
     */
    public function hasSameOriginAs(BoardingCardInterface $card): bool
    {
        return $this->departurePlace->getName() === $card->getArrivalPlace()->getName();
    }

    /**
     * @param BoardingCardInterface $card
     *
     * @return bool
     */
    public function hasSameDestinationAs(BoardingCardInterface $card): bool
    {
        return $this->arrivalPlace->getName() === $card->getDeparturePlace()->getName();
    }
}

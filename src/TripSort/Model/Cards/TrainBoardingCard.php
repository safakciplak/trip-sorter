<?php

namespace TripSort\Model\Cards;

use TripSort\Model\Place\Place;

/**
 * @author Safak Ciplak<safakciplak1990@gmail.com>
 */
class TrainBoardingCard implements BoardingCardInterface
{
    use BoardingCardTrait;

    /**
     * @var string
     */
    private $idTrain;

    /**
     * TrainBoardingCard constructor.
     *
     * @param Place $departurePlace
     * @param Place $arrivalPlace
     * @param string $seat
     * @param string $idTrain
     */
    public function __construct(
        Place $departurePlace,
        Place $arrivalPlace,
        string $seat,
        string $idTrain
    ) {
        $this->departurePlace = $departurePlace;
        $this->arrivalPlace = $arrivalPlace;
        $this->seat = $seat;
        $this->idTrain = $idTrain;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf(
            'Take train %s from %s to %s. Seat : %s.',
            $this->idTrain,
            $this->departurePlace,
            $this->arrivalPlace,
            $this->seat
        );
    }
}

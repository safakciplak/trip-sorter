<?php

namespace TripSort\Model\Cards;

use TripSort\Model\Place\Place;

/**
 * @author Safak Ciplak<safakciplak1990@gmail.com>
 */
class FlightBoardingCard implements BoardingCardInterface
{
    use BoardingCardTrait;

    /**
     * @var string
     */
    private $idFlight;
    /**
     * @var string
     */
    private $gate;
    /**
     * @var string
     */
    private $baggageTicketCounter;

    /**
     * FlightBoardingCard constructor.
     *
     * @param Place $departurePlace
     * @param Place $arrivalPlace
     * @param string $seat
     * @param string $idFlight
     * @param string $gate
     * @param string|null $baggageTicketCounter
     */
    public function __construct(
        Place $departurePlace,
        Place $arrivalPlace,
        string $seat,
        string $idFlight,
        string $gate,
        string $baggageTicketCounter = null
    ) {
        $this->departurePlace = $departurePlace;
        $this->arrivalPlace = $arrivalPlace;
        $this->seat = $seat;
        $this->idFlight = $idFlight;
        $this->gate = $gate;
        $this->baggageTicketCounter = $baggageTicketCounter ?: 'Automatically transferred';
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf(
            'Take the flight %s from %s to %s. Gate : %s. Seat : %s. Baggage ticket counter : %s.',
            $this->idFlight,
            $this->departurePlace,
            $this->arrivalPlace,
            $this->gate,
            $this->seat,
            $this->baggageTicketCounter
        );
    }
}

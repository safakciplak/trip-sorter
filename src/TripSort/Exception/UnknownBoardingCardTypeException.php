<?php

namespace TripSort\Exception;

/**
 * @author Safak Ciplak<safakciplak1990@gmail.com>
 */
/**
 * Class UnknownBoardingCardTypeException
 * @package TripSort\Exception
 */
class UnknownBoardingCardTypeException extends \RuntimeException
{
}

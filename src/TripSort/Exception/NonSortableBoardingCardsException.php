<?php

namespace TripSort\Exception;

/**
 * @author Safak Ciplak<safakciplak1990@gmail.com>
 */
/**
 * Class NonSortableBoardingCardsException
 * @package TripSort\Exception
 */
class NonSortableBoardingCardsException extends \RuntimeException
{
}

<?php

namespace TripSort\Service\Loader;

/**
 * New loaders have to implement this interface.
 *
 * @author Safak Ciplak<safakciplak1990@gmail.com>
 */
/**
 * Interface BoardingCardLoaderInterface
 * @package TripSort\Service\Loader
 */
interface BoardingCardLoaderInterface
{
    /**
     * @param string $data
     *
     * @return array
     */
    public function loadCards(string $data): array;
}

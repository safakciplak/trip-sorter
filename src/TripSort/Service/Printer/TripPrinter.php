<?php

namespace TripSort\Service\Printer;

use TripSort\Trip;

/**
 * Class TripPrinter
 * @package TripSort\Service\Printer
 */
class TripPrinter
{
    /**
     * @param Trip $trip
     */
    public function print(Trip $trip): void
    {
        echo $this->getPrintableFormat($trip);
    }

    /**
     * @param Trip $trip
     *
     * @return string
     */
    public function getPrintableFormat(Trip $trip): string
    {
        $output = null;

        foreach ($trip->getOrderedBoardingCards() as $boardingCard) {
            $output .= $boardingCard.PHP_EOL;
        }

        $output .= 'You have arrived at your final destination.';

        return $output;
    }
}

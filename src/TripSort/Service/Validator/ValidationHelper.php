<?php

namespace TripSort\Service\Validator;

/**
 * @author Safak Ciplak<safakciplak1990@gmail.com>
 */
/**
 * Class ValidationHelper
 * @package TripSort\Service\Validator
 */
class ValidationHelper
{
    /**
     * @param array $array
     * @param array $keys
     *
     * @return bool
     */
    public static function arrayKeysExists(array $array, array $keys): bool
    {
        return 0 === count(array_diff($keys, array_keys($array)));
    }
}

<?php

include "autoloader.php";
use TripSort\Model\Cards\BusBoardingCard;
use TripSort\Model\Cards\FlightBoardingCard;
use TripSort\Model\Place\Place;
use TripSort\Trip;

$card1 = new BusBoardingCard(
    new Place('London'),
    new Place('Keynes'),
    '3G'
);

$card2 = new BusBoardingCard(
    new Place('Keynes'),
    new Place('Newcastle'),
    '33A'
);

$card3 = new FlightBoardingCard(
    new Place('Newcastle'),
    new Place('Warsaw'),
    '12C',
    'NEWA334',
    '3A',
    '4B'
);


$trips = new Trip([$card3, $card2, $card1]);

$printer = new TripSort\Service\Printer\TripPrinter;

$results = $printer->print($trips);

echo '<pre>'; print_r($results); die;


